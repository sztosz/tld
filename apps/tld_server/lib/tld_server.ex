defmodule TLDServer do
  alias TLDServer.TLD

  def lookup(tld) when is_binary(tld) do
    TLD.lookup(:tld, tld)
  end

  def lookup(_tld) do
    {:error, "TLD has to be a string"}
  end

  def update do
    TLD.update(:tld)
  end

  def set(new_tld_map) when is_map(new_tld_map) do
    TLD.set(:tld, new_tld_map)
  end

  def set(_pid, _new_tld_map) do
    {:error, "TLD map has to be a map"}
  end
end
