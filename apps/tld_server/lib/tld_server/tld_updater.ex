defmodule TLDServer.TLDUpdater do
  require Logger

  @_200 "Got TLD list from icann server"
  @_404 "Got 404 when trying to get TLD list from icann server"
  @_update_ok "Replaced TLD list with new data"

  def update do
    spawn(__MODULE__, :get_tld, [])
  end

  def get_tld do
    HTTPoison.start
    case HTTPoison.get(Application.fetch_env!(:tld_server, :icann_url)) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        Logger.info(@_200)
        tld_map = parse_to_map(body)
        TLDServer.set(tld_map)
        Logger.info(@_update_ok)
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        Logger.error(@_404)
      {:error, %HTTPoison.Error{reason: reason}} ->
        Logger.error("Got #{reason} error when trying to get TLD list from icann server")
    end
  end

  defp parse_to_map(body) when is_binary(body) do
    body
    |> String.split("\n")
    |> Enum.reject(&(String.first(&1) == "#"))
    |> Enum.reduce(%{}, fn key, acc -> Map.put(acc, String.downcase(key), :ok) end)
  end
end
