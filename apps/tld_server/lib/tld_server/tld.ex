defmodule TLDServer.TLD do
  alias TLDServer.TLDUpdater
  alias TLDServer.TLD

  use GenServer

  # Public API

  def start_link(name) do
    GenServer.start_link(__MODULE__, %{}, name: name)
  end

  def lookup(pid, tld) when is_binary(tld) do
    GenServer.call(pid, {:lookup, String.downcase(tld)})
  end

  def update(pid) do
    GenServer.cast(pid, :update)
  end

  def set(pid, new_tld_map) when is_map(new_tld_map) do
    GenServer.cast(pid, {:set, new_tld_map})
  end

  # GenServer callbacks
  def init(tld_map) do
    TLD.update(self())
    {:ok, tld_map}
  end

  def handle_call({:lookup, tld}, _from, tld_map) do
    {:reply, Map.fetch(tld_map, tld), tld_map}
  end

  def handle_cast(:update, tld_map) do
    TLDUpdater.update()
    {:noreply, tld_map}
  end

  def handle_cast({:set, new_tld_map}, _tld_map) do
    {:noreply, new_tld_map}
  end
end
