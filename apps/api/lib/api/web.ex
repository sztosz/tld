defmodule API.Web do
  use Plug.Router
  require Logger

  plug Plug.Logger
  plug :match
  plug :dispatch

  def init(options) do
    options
  end

  def start_link do
    {:ok, _} = Plug.Adapters.Cowboy.http(API.Web, [])
  end

  get "/update" do
    TLDServer.update()
    conn
    |> send_resp(200, "Sent update request")
    |> halt
  end

  get "/:tld" do
    case TLDServer.lookup(tld) do
      {:ok, :ok} ->
        conn
        |> send_resp(200, "#{tld} is proper TLD")
        |> halt
      :error -> 
        conn
        |> send_resp(404, "TLD #{tld} was not found")
        |> halt
    end
  end

  match _ do
    conn
    |> send_resp(404, "Nothing here")
    |> halt
  end
end
