#!/usr/bin/env bash
docker build --tag=build-tld -f docker/Dockerfile.build.elixir .
docker run -v $PWD/releases:/app/releases build-tld mix release --env=prod
docker build --tag=run-tld -f docker/Dockerfile.run.elixir .